gitwatcher-ui
=============

git repo watcher-ui to pull, push, commit, fetch commands are all in one app. As gitHub application. written on Kivy

Requirements
------------
- git+https://github.com/RedXBeard/kivy.git@red
- watchdog

Installation on Linux
---------------------
pip install git+https://github.com/RedXBeard/kivy.git@red
pip install watchdog

Run on Linux(untill .deb package)
---------------------------------
python path-to-repofolder/main.py

Installation on Mac
-------------------
download and install <code>https://app.box.com/s/6dqltq9341x15ee32off</code>

Installing on Windows
---------------------
download and install <code>https://app.box.com/s/ae5aozoiu2qiyns1wwuy</code>